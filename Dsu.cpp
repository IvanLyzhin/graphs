#include "Dsu.h"

int Dsu::find(int item)
{
	if (parent[item] == item)
		return item;
	return parent[item] = find(parent[item]);
}

void Dsu::unite(int item1, int item2)
{
	int par1 = find(item1);
	int par2 = find(item2);
	
	if(par1!=par2)
	{
		if (rank[par1] < rank[par2])
			std::swap(par1, par2);
		parent[par2] = par1;
		rank[par1] += rank[par2];
	}
}
