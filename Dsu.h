#ifndef DSU_H
#define DSU_H
#include <vector>

class Dsu
{
public:
	explicit Dsu(int size)
	{
		parent.resize(size);
		rank.assign(size, 1);
		for (int i = 0; i < size; ++i)
			parent[i]=i;
	}

	int find(int item);

	void unite(int item1, int item2);

private:
	std::vector<int> parent;
	std::vector<int> rank;
};

#endif // DSU_H